<?php

/**
 * @file
 * Admin functions for Aramex.
 */

/**
 * Builds the admin settings form for configuring Aramex.
 *
 * @return array
 *   Drupal form for Aramex settings.
 */
function aramex_api_settings_form($form, &$form_state) {
  $form = array();
  // Client info.
  $form['info'] = array(
    '#title' => t('Client Info'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
  );
  $form['info']['aramex_api_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Aramex User Name'),
    '#default_value' => variable_get('aramex_api_username'),
  );
  $form['info']['aramex_api_password'] = array(
    '#type' => 'password',
    '#title' => t('Aramex Password'),
    '#description' => variable_get('aramex_api_password') ? t('Aramex password has been set. Leave blank unless you want to change the saved password.') : NULL,
  );
  $form['info']['aramex_api_account_number'] = array(
    '#type' => 'textfield',
    '#title' => t('Aramex Account Number'),
    '#default_value' => variable_get('aramex_api_account_number'),
  );
  $form['info']['aramex_api_account_pin'] = array(
    '#type' => 'textfield',
    '#title' => t('Aramex Account Pin'),
    '#default_value' => variable_get('aramex_api_account_pin'),
  );
  $form['info']['aramex_api_account_entity'] = array(
    '#type' => 'textfield',
    '#title' => t('Aramex Account Entity'),
    '#default_value' => variable_get('aramex_api_account_entity'),
  );
  $form['info']['aramex_api_account_country'] = array(
    '#type' => 'textfield',
    '#title' => t('Aramex Account Country Code'),
    '#default_value' => variable_get('aramex_api_account_country'),
  );
  $form['info']['aramex_api_request_mode'] = array(
    '#type' => 'radios',
    '#title' => t('Aramex Request Mode'),
    '#options' => array(
      'testing' => t('Testing'),
      'production' => t('Production'),
    ),
    '#default_value' => variable_get('aramex_api_request_mode', 'testing'),
  );

  // Origin address.
  $form['origin'] = array(
    '#type' => 'fieldset',
    '#title' => t('Ship From Address'),
    '#collapsible' => TRUE,
  );
  $form['origin']['aramex_api_company_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Company Name'),
    '#default_value' => variable_get('aramex_api_company_name'),
  );
  $form['origin']['aramex_api_address_line_1'] = array(
    '#type' => 'textfield',
    '#title' => t('Address Line 1'),
    '#required' => TRUE,
    '#default_value' => variable_get('aramex_api_address_line_1'),
  );
  $form['origin']['aramex_api_address_line_2'] = array(
    '#type' => 'textfield',
    '#title' => t('Address Line 2 (Additional)'),
    '#default_value' => variable_get('aramex_api_address_line_2'),
  );
  $form['origin']['aramex_api_address_line_3'] = array(
    '#type' => 'textfield',
    '#title' => t('Address Line 3 (Additional)'),
    '#default_value' => variable_get('aramex_api_address_line_3'),
  );
  $form['origin']['aramex_api_address_city'] = array(
    '#type' => 'textfield',
    '#title' => t('City'),
    '#required' => TRUE,
    '#default_value' => variable_get('aramex_api_address_city'),
  );
  $form['origin']['aramex_api_address_state'] = array(
    '#type' => 'textfield',
    '#title' => t('State or Province'),
    '#description' => t('If shipping from USA or Canada, enter the 2 character abbreviation for the shipping State or Province.'),
    '#default_value' => variable_get('aramex_api_address_state'),
    '#size' => 2,
  );
  $form['origin']['aramex_api_address_postal_code'] = array(
    '#type' => 'textfield',
    '#title' => t('Postal Code'),
    '#description' => t('Enter your postal code if available.'),
    '#size' => 12,
    '#default_value' => variable_get('aramex_api_address_postal_code'),
  );
  $form['origin']['aramex_api_address_country_code'] = array(
    '#type' => 'select',
    '#title' => t('Country'),
    '#default_value' => variable_get('aramex_api_address_country_code'),
    '#options' => array('' => t('Select Country')) + aramex_api_country_codes(),
    '#required' => TRUE,
  );

  // Service types and product types.
  $form['services'] = array(
    '#type' => 'fieldset',
    '#title' => t('Enabled Aramex Shipping Services'),
    '#collapsible' => TRUE,
  );
  $form['services']['aramex_api_service_types'] = array(
    '#title' => t('Service Types'),
    '#type' => 'checkboxes',
    '#options' => aramex_api_service_types(),
    '#default_value' => variable_get('aramex_api_service_types', array()),
  );
  $form['services']['aramex_api_service_codes'] = array(
    '#title' => t('Service Codes'),
    '#type' => 'checkboxes',
    '#options' => aramex_api_service_codes(),
    '#default_value' => variable_get('aramex_api_service_codes', array()),
  );
  $form['services']['aramex_api_payment_methods'] = array(
    '#title' => t('Payment Methods'),
    '#type' => 'checkboxes',
    '#options' => aramex_api_payment_methods(),
    '#default_value' => variable_get('aramex_api_payment_methods', array()),
  );

  $form['package'] = array(
    '#type' => 'fieldset',
    '#title' => t('Default Shipment Details'),
    '#collapsible' => TRUE,
  );

  $form['package']['weight'] = array(
    '#type' => 'fieldset',
    '#title' => t('Default Weight'),
    '#collapsible' => FALSE,
  );
  $form['package']['weight']['aramex_api_package_weight_value'] = array(
    '#type' => 'textfield',
    '#title' => t('Value'),
    '#required' => TRUE,
    '#maxlength' => 7,
    '#size' => 5,
    '#default_value' => variable_get('aramex_api_package_weight_value', ''),
  );
  $form['package']['weight']['aramex_api_package_weight_unit'] = array(
    '#type' => 'select',
    '#title' => t('Unit'),
    '#required' => TRUE,
    '#options' => array('KG' => t('KG'), 'LB' => t('LB')),
    '#default_value' => variable_get('aramex_api_package_weight_unit', 'KG'),
  );

  $form['package']['dimensions'] = array(
    '#type' => 'fieldset',
    '#title' => t('Default Package Dimensions'),
    '#collapsible' => FALSE,
  );
  $form['package']['dimensions']['aramex_api_package_dimensions_length'] = array(
    '#type' => 'textfield',
    '#title' => t('Length'),
    '#maxlength' => 5,
    '#size' => 5,
    '#default_value' => variable_get('aramex_api_package_dimensions_length', ''),
  );
  $form['package']['dimensions']['aramex_api_package_dimensions_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Width'),
    '#maxlength' => 5,
    '#size' => 5,
    '#default_value' => variable_get('aramex_api_package_dimensions_width', ''),
  );
  $form['package']['dimensions']['aramex_api_package_dimensions_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Height'),
    '#maxlength' => 5,
    '#size' => 5,
    '#default_value' => variable_get('aramex_api_package_dimensions_height', ''),
  );
  $form['package']['dimensions']['aramex_api_package_dimensions_unit'] = array(
    '#type' => 'select',
    '#title' => t('Unit'),
    '#options' => array('CM' => t('CM'), 'M' => t('M')),
    '#default_value' => variable_get('aramex_api_package_dimensions_unit', 'CM'),
  );

  $form['package']['volume'] = array(
    '#type' => 'fieldset',
    '#title' => t('Default Package Volume'),
    '#collapsible' => FALSE,
  );
  $form['package']['volume']['aramex_api_package_volume_value'] = array(
    '#type' => 'textfield',
    '#title' => t('Value'),
    '#required' => TRUE,
    '#maxlength' => 7,
    '#size' => 5,
    '#default_value' => variable_get('aramex_api_package_volume_value', ''),
  );
  $form['package']['volume']['aramex_api_package_volume_unit'] = array(
    '#type' => 'select',
    '#title' => t('Unit'),
    '#required' => TRUE,
    '#options' => array('Cm3' => t('Cm3'), 'Inch3' => t('Inch3')),
    '#default_value' => variable_get('aramex_api_package_volume_unit', 'Cm3'),
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );
  return $form;
}

/**
 * Validate handler for Aramex admin settings form.
 */
function aramex_api_settings_form_validate($form, &$form_state) {
  // Validate Aramex settings that are required from USA and Canada only.
  if (in_array($form_state['values']['aramex_api_address_country_code'], array('US', 'CA'))) {
    // Make sure state is provided.
    if (empty($form_state['values']['aramex_api_address_state'])) {
      form_set_error('aramex_api_address_state', t('State or Province is required when shipping from USA or Canada.'));
    }
    else {
      // Make sure state is 2 characters.
      if (!preg_match('/^[a-z][a-z]$/i', $form_state['values']['aramex_api_address_state'])) {
        form_set_error('aramex_api_address_state', t('Please enter the 2 character abbreviation for your State or Province'));
      }
    }

    // Make sure postal code is provided.
    if (empty($form_state['values']['aramex_api_postal_code'])) {
      form_set_error('aramex_api_postal_code', t('Postal code is required when shipping from USA or Canada.'));
    }
  }
}

/**
 * Submit handler for Aramex admin settings form.
 */
function aramex_api_settings_form_submit($form, &$form_state) {
  $variables = $form_state['values'];
  unset($variables['submit'], $variables['form_build_id'], $variables['form_token'], $variables['form_id'], $variables['op']);

  // Make sure State/Province value is saved as uppercase.
  if (!empty($variables['aramex_api_address_state'])) {
    $variables['aramex_api_address_state'] = drupal_strtoupper($variables['aramex_api_address_state']);
  }

  // Loop through each of the settings fields and save their values.
  foreach ($variables as $key => $value) {
    if (!empty($value)) {
      variable_set($key, $value);
    }
    else {
      // If the value is not set then delete the variable instead of saving
      // a NULL value (unless it's the Aramex password which is only set when
      // it is updated).
      if ($key != 'aramex_api_password') {
        variable_del($key);
      }
    }
  }

  drupal_set_message(t('The Aramex configuration options have been saved.'));
}
