CONTENTS OF THIS FILE
---------------------
  
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * FAQ
 * Maintainers


INTRODUCTION
------------
Aramex Shipping Services Module

This module provides the base foundation to connect to 
aramex shipping services API.
Inside this module files you will find testing modules and code samples 
to create an aramex request to do the following:
  - Rate Calculation
  - Track Shipment
  - Create Shipment
  - Print Label
  - Create Pickup
  - Cancel Pickup

Please note that this module only handles the API calls to aramex servers. 
You need to write your own code to collect the required parameters to send 
an aramex request.
Please refere to this URL to get more info and to download documentation files: 
http://www.aramex.com/developers/


REQUIREMENTS
------------
This module does not require any other modules.


INSTALLATION
------------
 * Download the module from Drupal community
 * Extract the content of the downloaded file inside your modules folder
 * Enable the module
 * If you don't already have an aramex account, goto: 
    https://www.aramex.com/accounts/registration-signup.aspx and register 
    for an account
 * Goto /admin/config/system/aramex and fill in the required info
 * To use the test modules you need to fill inside the settings with 
    the following info:
        Account Country Code =JO
        Account Entity = AMM
        Account Number= 20016
        Account Pin = 331421
        UserName = testingapi@aramex.com
        Password = R123456789$r
7. Write your own module or code to use the functions inside the api module


CONFIGURATION
-------------
 * Configure your permissions in Administration » People » Permissions:
	- Change Aramex API Configuarations
		give to authinticated users only

 * Goto /admin/config/system/aramex and fill in your account info


MAINTAINERS
-----------
Current maintainers:
 * Mohammad Bdour (https://www.drupal.org/user/1345562)

This project has been sponsored by:
 * ARAMEX (http://www.aramex.com)
	 Aramex is a leading global provider of comprehensive logistics and
	 transportation solutions. Established in 1982 as an express operator,
   the company rapidly evolved into a global brand recognized for its 
   customized services and innovative multi-product offering. In January 1997,
   Aramex became the first Arab-based international company to trade its
   shares on the NASDAQ stock exchange.
	 The range of services offered by Aramex includes international and
   domestic express delivery, freight forwarding, logistics and warehousing,
   records and information Management solutions, e-business solutions,
   and online shopping services.
