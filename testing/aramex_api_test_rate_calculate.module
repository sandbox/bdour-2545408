<?php

/**
 * @file
 * Testing Module: to test the rate calculation for aramex shipping.
 */

/**
 * Implements hook_menu().
 */
function aramex_api_test_rate_calculate_menu() {
  $items = array();
  $items['aramex-test/rate-calculate'] = array(
    'title' => 'Test Aramex Rate Calculation',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('aramex_api_test_rate_calculate_test_form'),
    'access arguments' => array('access content'),
    'type' => MENU_NORMAL_ITEM,
    'weight' => 0,
  );

  return $items;
}

/**
 * Testing form.
 */
function aramex_api_test_rate_calculate_test_form($form, &$form_state) {
  $form['tabs'] = array(
    '#type' => 'vertical_tabs',
  );
  // Ship from address.
  $form['OriginAddress'] = array(
    '#type' => 'fieldset',
    '#title' => t('Ship From Address'),
    '#collapsible' => TRUE,
    '#group' => 'tabs',
    '#tree' => TRUE,
  );
  $form['OriginAddress']['Line1'] = array(
    '#type' => 'textfield',
    '#title' => t('Address Line 1'),
    '#required' => TRUE,
    '#maxlength' => 50,
    '#default_value' => variable_get('aramex_api_address_line_1'),
  );
  $form['OriginAddress']['Line2'] = array(
    '#type' => 'textfield',
    '#title' => t('Address Line 2 (Additional)'),
    '#maxlength' => 50,
    '#default_value' => variable_get('aramex_api_address_line_2'),
  );
  $form['OriginAddress']['Line3'] = array(
    '#type' => 'textfield',
    '#title' => t('Address Line 3 (Additional)'),
    '#maxlength' => 50,
    '#default_value' => variable_get('aramex_api_address_line_3'),
  );
  $form['OriginAddress']['City'] = array(
    '#type' => 'textfield',
    '#title' => t('City'),
    '#required' => TRUE,
    '#maxlength' => 50,
    '#default_value' => variable_get('aramex_api_address_city'),
  );
  $form['OriginAddress']['StateOrProvinceCode'] = array(
    '#type' => 'textfield',
    '#title' => t('State or Province'),
    '#description' => t('If shipping from USA or Canada, enter the 2 character abbreviation for the shipping State or Province.'),
    '#maxlength' => 100,
    '#default_value' => variable_get('aramex_api_address_state'),
    '#size' => 2,
  );
  $form['OriginAddress']['PostCode'] = array(
    '#type' => 'textfield',
    '#title' => t('Postal Code'),
    '#description' => t('Enter your postal code if available.'),
    '#size' => 12,
    '#maxlength' => 30,
    '#default_value' => variable_get('aramex_api_address_postal_code'),
  );
  $form['OriginAddress']['CountryCode'] = array(
    '#type' => 'select',
    '#title' => t('Country'),
    '#default_value' => variable_get('aramex_api_address_country_code'),
    '#options' => array('' => t('Select Country')) + aramex_api_country_codes(),
    '#required' => TRUE,
  );

  $form['DestinationAddress'] = array(
    '#type' => 'fieldset',
    '#title' => t('Ship To Address'),
    '#collapsible' => TRUE,
    '#tree' => TRUE,
    '#group' => 'tabs',
  );
  $form['DestinationAddress']['Line1'] = array(
    '#type' => 'textfield',
    '#title' => t('Address Line 1'),
    '#required' => TRUE,
    '#maxlength' => 50,
  );
  $form['DestinationAddress']['Line2'] = array(
    '#type' => 'textfield',
    '#title' => t('Address Line 2 (Additional)'),
    '#maxlength' => 50,
  );
  $form['DestinationAddress']['Line3'] = array(
    '#type' => 'textfield',
    '#title' => t('Address Line 3 (Additional)'),
    '#maxlength' => 50,
  );
  $form['DestinationAddress']['City'] = array(
    '#type' => 'textfield',
    '#title' => t('City'),
    '#required' => TRUE,
    '#maxlength' => 100,
  );
  $form['DestinationAddress']['StateOrProvinceCode'] = array(
    '#type' => 'textfield',
    '#title' => t('State or Province'),
    '#description' => t('If shipping from USA or Canada, enter the 2 character abbreviation for the shipping State or Province.'),
    '#size' => 2,
    '#maxlength' => 100,
  );
  $form['DestinationAddress']['PostCode'] = array(
    '#type' => 'textfield',
    '#title' => t('Postal Code'),
    '#description' => t('Enter your postal code if available.'),
    '#size' => 12,
    '#maxlength' => 30,
  );
  $form['DestinationAddress']['CountryCode'] = array(
    '#type' => 'select',
    '#title' => t('Country'),
    '#options' => array('' => t('Select Country')) + aramex_api_country_codes(),
    '#required' => TRUE,
  );

  $form['Transaction'] = array(
    '#type' => 'fieldset',
    '#title' => t('Comments'),
    '#collapsible' => TRUE,
    '#group' => 'tabs',
    '#tree' => TRUE,
  );
  $form['Transaction']['Reference1'] = array(
    '#type' => 'textfield',
    '#title' => t('Comment 1'),
    '#maxlength' => 50,
  );
  $form['Transaction']['Reference2'] = array(
    '#type' => 'textfield',
    '#title' => t('Comment 2'),
    '#maxlength' => 50,
  );

  $form['ShipmentDetails'] = array(
    '#type' => 'fieldset',
    '#title' => t('Shipment Details'),
    '#collapsible' => TRUE,
    '#group' => 'tabs',
    '#tree' => TRUE,
  );
  $form['ShipmentDetails']['ActualWeight'] = array(
    '#type' => 'fieldset',
    '#title' => t('Actual Weight'),
    '#collapsible' => FALSE,
    '#tree' => TRUE,
  );
  $form['ShipmentDetails']['ActualWeight']['Value'] = array(
    '#type' => 'textfield',
    '#title' => t('Value'),
    '#required' => TRUE,
    '#maxlength' => 7,
    '#size' => 2,
  );
  $form['ShipmentDetails']['ActualWeight']['Unit'] = array(
    '#type' => 'select',
    '#title' => t('Unit'),
    '#required' => TRUE,
    '#options' => array('KG' => t('KG'), 'LB' => t('LB')),
    '#default_value' => 'KG',
  );
  $form['ShipmentDetails']['ChargeableWeight'] = array(
    '#type' => 'fieldset',
    '#title' => t('Chargeable Weight'),
    '#collapsible' => FALSE,
    '#tree' => TRUE,
  );
  $form['ShipmentDetails']['ChargeableWeight']['Value'] = array(
    '#type' => 'textfield',
    '#title' => t('Value'),
    '#required' => TRUE,
    '#maxlength' => 7,
    '#size' => 2,
  );
  $form['ShipmentDetails']['ChargeableWeight']['Unit'] = array(
    '#type' => 'select',
    '#title' => t('Unit'),
    '#required' => TRUE,
    '#options' => array('KG' => t('KG'), 'LB' => t('LB')),
    '#default_value' => 'KG',
  );
  $form['ShipmentDetails']['NumberOfPieces'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of Pieces'),
    '#maxlength' => 3,
    '#size' => 2,
    '#required' => TRUE,
  );
  $form['ShipmentDetails']['ProductGroup'] = array(
    '#type' => 'select',
    '#title' => t('Product Group'),
    '#required' => TRUE,
    '#options' => array('EXP' => t('Express'), 'DOM' => t('Domestic')),
  );
  $form['ShipmentDetails']['ProductType'] = array(
    '#type' => 'select',
    '#title' => t('Product Type'),
    '#required' => TRUE,
    '#options' => aramex_api_enabled_service_types(),
  );
  $form['ShipmentDetails']['PaymentType'] = array(
    '#type' => 'select',
    '#title' => t('Payment Type'),
    '#required' => TRUE,
    '#options' => aramex_api_enabled_payment_methods(),
  );
  $form['ShipmentDetails']['DescriptionOfGoods'] = array(
    '#type' => 'textfield',
    '#title' => t('Description of Goods'),
    '#required' => TRUE,
    '#maxlength' => 100,
  );
  $form['ShipmentDetails']['GoodsOriginCountry'] = array(
    '#type' => 'select',
    '#title' => t('Goods Origin Country'),
    '#options' => aramex_api_country_codes(),
    '#required' => TRUE,
  );

  if (($form_state['submitted'] == 1) && isset($form_state['values']['response'])) {
    $form['tabs']['#default_tab'] = 'edit-result';

    $form['result'] = array(
      '#type' => 'fieldset',
      '#title' => t('Aramex Response'),
      '#collapsible' => TRUE,
      '#group' => 'tabs',
      '#tree' => TRUE,
    );
    if ($form_state['values']['response']['has_errors'] == 0) {
      $form['result']['submission'] = array(
        '#markup' => $form_state['values']['response']['text'],
      );
    }
    else {
      $error_list = '<br />' . implode('<br />', $form_state['values']['response']['errors']);
      $form['result']['submission'] = array(
        '#markup' => t('There was the following errors in your request: !error_list', array('!error_list' => $error_list)),
      );
    }
  }

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Calculate Rates'),
  );
  return $form;
}

/**
 * Testing form validation.
 */
function aramex_api_test_rate_calculate_test_form_validate(&$form, &$form_state) {
  if (($form_state['values']['OriginAddress']['CountryCode'] == 'US') ||
      ($form_state['values']['OriginAddress']['CountryCode'] == 'CA')) {
    if (empty($form_state['values']['OriginAddress']['StateOrProvinceCode'])) {
      form_set_error('OriginAddress][StateOrProvinceCode', t('State or Provice Code is required for US and CA'));
    }
  }
  if (($form_state['values']['DestinationAddress']['CountryCode'] == 'US') ||
      ($form_state['values']['DestinationAddress']['CountryCode'] == 'CA')) {
    if (empty($form_state['values']['DestinationAddress']['StateOrProvinceCode'])) {
      form_set_error('DestinationAddress][StateOrProvinceCode', t('State or Provice Code is required for US and CA'));
    }
  }

  if (!empty($form_state['values']['ShipmentDetails']['ActualWeight']['Value']) &&
      !filter_var($form_state['values']['ShipmentDetails']['ActualWeight']['Value'], FILTER_VALIDATE_FLOAT)) {
    form_set_error('ShipmentDetails][ActualWeight][Value', t('Actual Weight Value must be numeric.'));
  }
  if (!empty($form_state['values']['ShipmentDetails']['ChargeableWeight']['Value']) &&
      !filter_var($form_state['values']['ShipmentDetails']['ChargeableWeight']['Value'], FILTER_VALIDATE_FLOAT)) {
    form_set_error('ShipmentDetails][ChargeableWeight][Value', t('Chargeable Weight Value must be numeric.'));
  }
}

/**
 * Testing form submit.
 */
function aramex_api_test_rate_calculate_test_form_submit(&$form, &$form_state) {
  $params = array(
    'ClientInfo' => array(
      'UserName' => variable_get('aramex_api_username'),
      'Password' => variable_get('aramex_api_password'),
      'AccountNumber' => variable_get('aramex_api_account_number'),
      'AccountPin' => variable_get('aramex_api_account_pin'),
      'AccountEntity' => variable_get('aramex_api_account_entity'),
      'AccountCountryCode' => variable_get('aramex_api_account_country'),
      'Version' => '1.0',
    ),
    'OriginAddress' => $form_state['values']['OriginAddress'],
    'DestinationAddress' => $form_state['values']['DestinationAddress'],
    'Transaction' => $form_state['values']['Transaction'],
    'ShipmentDetails' => $form_state['values']['ShipmentDetails'],
  );

  $results = aramex_api_calculate_shipping_rates($params);

  if ($results) {
    if ($results->HasErrors == 1) {
      $errors = array();
      if (is_array($results->Notifications->Notification)) {
        foreach ($results->Notifications->Notification as $notification) {
          $errors[] = $notification->Message;
        }
      }
      else {
        $errors[] = $results->Notifications->Notification->Message;
      }
      $form_state['values']['response']['has_errors'] = 1;
      $form_state['values']['response']['errors'] = $errors;
    }
    else {
      $form_state['values']['response']['has_errors'] = 0;
      $form_state['values']['response']['text'] = t('The Calculated Rate is: @value @currency.', array(
        '@value' => $results->TotalAmount->Value,
        '@currency' => $results->TotalAmount->CurrencyCode,
          )
      );
    }
  }
  else {
    drupal_set_message(t('There was an error in processing your request. Please check watchlog for more info.'), 'error');
  }

  $form_state['rebuild'] = TRUE;
}
